package nacmunana.inspedralbes.cat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor {
	public static final byte[] IV_PARAM = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B,
			0x0C, 0x0D, 0x0E, 0x0F };

	private static final String METHOD_AES = "AES";
	private static final String METHOD_DES = "DES";

	private static final String TYPE_ECB = "ECB";
	private static final String TYPE_CBC = "CBC";

	private static final String PADDING = "PKCS5Padding";
	private static final int LENGTH_56 = 64;
	private static final int LENGTH_128 = 128;
	private static final int LENGTH_192 = 192;
	private static final int LENGTH_256 = 256;

	private static final String HASH_MDS = "MD5";
	private static final String HASH_SHA1 = "SHA-1";
	private static final String HASH_SHA256 = "SHA-256";

	private static File file = new File("prueba.txt");
	private static String fileCrytpoName = "prueba.txt.cry";

	public static void main(String[] args) {
		// Crea la key
		SecretKey sKey = passwordKeyGeneration("aceituna", HASH_MDS, METHOD_DES, LENGTH_56);

		// Get bytes del contingut del fitxer, els encripta i els escriu en un fitxer
		byte[] fileContent = readFile(file);
		byte[] datosEncriptados = encryptDecryptData(file.getName(), METHOD_DES, TYPE_ECB, PADDING, sKey, fileContent);
		writeFile(datosEncriptados, fileCrytpoName);

		// Esborra el fitxer sense encriptar
		try {
			//wipe(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		file.delete();

		// Llegeix fitxer, desencripta el contingut i el mostra per pantalla
		fileContent = readFile(new File(fileCrytpoName));
		byte[] datosDesencriptados = encryptDecryptData(fileCrytpoName, METHOD_DES, TYPE_ECB, PADDING, sKey,
				fileContent);
		System.out.println(new String(datosDesencriptados, StandardCharsets.UTF_8));

	}

	public static SecretKey passwordKeyGeneration(String text, String uHash, String uMethod, int keySize) {
		SecretKey sKey = null;
		try {
			byte[] data = text.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance(uHash);
			byte[] hash = md.digest(data);
			byte[] key = Arrays.copyOf(hash, keySize / 8);
			sKey = new SecretKeySpec(key, uMethod);
		} catch (Exception ex) {
			System.err.println("Error generant la clau:" + ex);
		}
		return sKey;
	}

	public static byte[] encryptDecryptData(String fileName, String method, String blocType, String padding,
			SecretKey sKey, byte[] data) {
		byte[] encryptedData = null;
		int mode;
		if (fileName.endsWith(".cry"))
			mode = Cipher.DECRYPT_MODE;
		else
			mode = Cipher.ENCRYPT_MODE;
		try {
			Cipher cipher = Cipher.getInstance(method + "/" + blocType + "/" + padding);
			if (blocType.equals(TYPE_ECB)) {
				cipher.init(mode, sKey);
			} else if (blocType.equals(TYPE_CBC)) {
				IvParameterSpec iv = null;
				if (method.equals(METHOD_AES))
					iv = new IvParameterSpec(IV_PARAM);
				else if (method.equals(METHOD_DES))
					iv = new IvParameterSpec(Arrays.copyOf(IV_PARAM, IV_PARAM.length / 2));
				cipher.init(mode, sKey, iv);
			}
			encryptedData = cipher.doFinal(data);
		} catch (Exception ex) {
			System.err.println("Error xifrant les dades: " + ex);
		}
		return encryptedData;
	}

	public static byte[] readFile(File file) {
		byte[] data = new byte[(int) file.length()];
		byte[] res = null;
		try (FileInputStream in = new FileInputStream(file)) {
			int read = in.read(data);
			while (read > 0) {
				res = data;
				read = in.read(data);
			}
		} catch (Exception e) {
			System.err.println("Error while reading data: " + e);
		}
		return res;
	}

	public static void writeFile(byte[] out, String fileName) {
		System.out.println(out[0]);
		try (FileOutputStream outFile = new FileOutputStream(fileName)) {
			outFile.write(out);
		} catch (Exception e) {
			System.err.println("Error while writing data: " + e);
		}
	}

	public static void wipe(File f) throws Exception {
		try (RandomAccessFile rand = new RandomAccessFile(f, "rw")) {
			for (int i = 0; i < rand.length(); i++)
				rand.write(0);
		}
	}

}
