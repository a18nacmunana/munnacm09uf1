package nacmunana.inspedralbes.cat;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.crypto.SecretKey;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class GUI extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField textField_password;
	private JTextField textField_source;
	private JTextField textField_destination;

	private JComboBox comboBox_method;
	private JComboBox comboBox_type;
	private JComboBox comboBox_padding;
	private JComboBox comboBox_keyLength;
	private JComboBox comboBox_hash;

	private JFileChooser chooser = new JFileChooser();
	private int returnVal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JLabel lblMethod = new JLabel("Method");
		panel.add(lblMethod);

		comboBox_method = new JComboBox();
		comboBox_method.setMaximumSize(new Dimension(32767, 20));
		comboBox_method.addItem("AES");
		comboBox_method.addItem("DES");
		panel.add(comboBox_method);

		JLabel lblType = new JLabel("Bloc Type");
		panel.add(lblType);

		comboBox_type = new JComboBox();
		comboBox_type.setMaximumSize(new Dimension(32767, 20));
		comboBox_type.addItem("ECB");
		comboBox_type.addItem("CBC");
		panel.add(comboBox_type);

		JLabel lblPadding = new JLabel("Padding");
		panel.add(lblPadding);

		comboBox_padding = new JComboBox();
		comboBox_padding.setMaximumSize(new Dimension(32767, 20));
		comboBox_padding.addItem("PKCS5Padding");
		panel.add(comboBox_padding);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_1.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_1);

		JLabel lblKeyLength = new JLabel("Key length");
		panel_1.add(lblKeyLength);

		comboBox_keyLength = new JComboBox();
		comboBox_keyLength.addItem("64");
		comboBox_keyLength.addItem("128");
		comboBox_keyLength.addItem("192");
		comboBox_keyLength.addItem("256");

		panel_1.add(comboBox_keyLength);

		JLabel lblHash = new JLabel("Hash");
		panel_1.add(lblHash);

		comboBox_hash = new JComboBox();
		comboBox_hash.addItem("MD5");
		comboBox_hash.addItem("SHA-1");
		comboBox_hash.addItem("SHA-256");
		panel_1.add(comboBox_hash);

		JLabel lblPassword = new JLabel("Key password");
		panel_1.add(lblPassword);

		textField_password = new JTextField();
		panel_1.add(textField_password);
		textField_password.setColumns(10);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_2);

		JLabel lblSource = new JLabel("Source");
		panel_2.add(lblSource);

		textField_source = new JTextField();
		panel_2.add(textField_source);
		textField_source.setColumns(20);

		JButton btnSelectSource = new JButton("Select");
		btnSelectSource.setActionCommand("source");
		btnSelectSource.addActionListener(this);
		panel_2.add(btnSelectSource);

		JPanel panel_2_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_2_1);

		JLabel lblDestination = new JLabel("Destination");
		panel_2_1.add(lblDestination);

		textField_destination = new JTextField();
		textField_destination.setColumns(20);
		panel_2_1.add(textField_destination);

		JButton btnSelectDestination = new JButton("Select");
		btnSelectDestination.setActionCommand("destination");
		btnSelectDestination.addActionListener(this);
		panel_2_1.add(btnSelectDestination);

		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3);

		JButton btnHazLoTuyo = new JButton("Encrypt/Decrypt");
		btnHazLoTuyo.setActionCommand("transform");
		btnHazLoTuyo.addActionListener(this);
		panel_3.add(btnHazLoTuyo);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch (arg0.getActionCommand()) {
		case "source":
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			returnVal = chooser.showOpenDialog(getParent());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File fitxer = chooser.getSelectedFile();
				String path = fitxer.getAbsolutePath();
				textField_source.setText(path);
			}
			break;

		case "destination":
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			returnVal = chooser.showOpenDialog(getParent());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File fitxer = chooser.getSelectedFile();
				String path = fitxer.getAbsolutePath();
				textField_destination.setText(path);
			}
			break;

		case "transform":

			String method = comboBox_method.getSelectedItem().toString();
			String type = comboBox_type.getSelectedItem().toString();
			String padding = comboBox_padding.getSelectedItem().toString();
			int keyLength = Integer.parseInt(comboBox_keyLength.getSelectedItem().toString());
			String hash = comboBox_hash.getSelectedItem().toString();
			String password = textField_password.getText();

			File sourceFile = new File(textField_source.getText());
			String destinationFileName = textField_destination.getText() + "/" + sourceFile.getName() + ".cry";

			if (sourceFile.getName().endsWith(".cry"))
				destinationFileName = textField_destination.getText() + "/" + sourceFile.getName().split(".cry")[0];

//			Encryptor encrypt = new Encryptor();
			SecretKey sKey = Encryptor.passwordKeyGeneration(password, hash, method, keyLength);

			byte[] fileContent = Encryptor.readFile(sourceFile);
			byte[] datosEncriptados = Encryptor.encryptDecryptData(sourceFile.getName(), method, type, padding, sKey,
					fileContent);
			Encryptor.writeFile(datosEncriptados, destinationFileName);

			break;
		}
	}

}
