package cat.inspedralbres.m9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class QuoteClientTCP {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String hostName = "localhost";
		int portNumber = 6363;
		boolean moreQuotes = true;
		
		System.setProperty("javax.net.ssl.trustStore", "selfsignedClient.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "ausias");		
		System.setProperty("javax.net.debug", "SSL,handshake");

		try {
			SSLSocketFactory sslFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			Socket serverSocket = (SSLSocket) sslFactory.createSocket(hostName, portNumber);
			BufferedReader in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
			PrintWriter out = new PrintWriter(serverSocket.getOutputStream(), true);
			do {
				String serverInput;
				System.out.println("Write a number");
				int num = scan.nextInt();
				out.println(num);
				serverInput=in.readLine();
				System.out.println("Server resposta: "+serverInput);
				
				System.out.println("Do you want to get a quote (Y|N)?");
				String answer = scan.next();
				moreQuotes = (answer.equals("Y"))? true : false;
			} while (moreQuotes);
			in.close();
			out.close();
			serverSocket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
