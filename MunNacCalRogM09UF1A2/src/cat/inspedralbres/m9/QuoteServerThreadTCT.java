package cat.inspedralbres.m9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class QuoteServerThreadTCT extends Thread {

	Socket clientSocket;
	HashMap<Integer, String> quotes;

	public QuoteServerThreadTCT(Socket socket, HashMap<Integer, String> quotes) {
		this.clientSocket = socket;
		this.quotes = quotes;
	}

	public void run() {

		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			System.out.println(clientSocket.isConnected());

			System.out.println(clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " is connected");
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);

				String frase = quotes.get(Integer.parseInt(inputLine));
				System.out.println("I'm going to send: " + frase);
				out.println(frase);
				// clientSocket.isConnected();
			}
			out.close();
			in.close();
			clientSocket.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
}
