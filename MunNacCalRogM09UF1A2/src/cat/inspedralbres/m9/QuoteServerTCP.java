package cat.inspedralbres.m9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class QuoteServerTCP {

	public static void main(String[] args) {
		int portNumber = 6363;
		boolean continuar = true;
		HashMap<Integer, String> quotes = new HashMap();
		System.setProperty("javax.net.ssl.keyStore", "selfsigned.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "ausias");	
		System.setProperty("javax.net.debug", "SSL,handshake");

		try {
			File file = new File("one-liners.txt");
			// System.out.println(file.exists());
			String returnValue = null;
			int numQuote = 1;
			BufferedReader fileInput = new BufferedReader(new FileReader(file));
			while ((returnValue = fileInput.readLine()) != null) {
				// System.out.println(returnValue);
				quotes.put(numQuote, returnValue);
				numQuote++;
			}
			fileInput.close();

			System.out.println("I have " + quotes.size() + " quotes");

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			SSLServerSocketFactory sslFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			SSLServerSocket serverSocket = (SSLServerSocket) sslFactory.createServerSocket(portNumber);
			
			while(continuar) {
				new QuoteServerThreadTCT(serverSocket.accept(), quotes).start();
			}
						
			serverSocket.close();

		} catch (IOException e) {
			System.out.println(
					"Exception caught when trying to listen on port " + portNumber + " or listening for a connection");
			System.out.println(e.getMessage());
		}
	}

}
